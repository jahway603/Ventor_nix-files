# Auf allen Desktops ...

{ config, pkgs, ... }:

{
  
  imports =
  [ # Include the results of the hardware scan.
      ../../general.nix
  ];
 

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "de";
    xkbVariant = "";
  };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     kate
     thunderbird
     firefox
     steam
     neofetch
     vlc
     pavucontrol
     ungoogled-chromium
     vscode
     monaspace
     cascadia-code
     spotify
     putty
     discord
     deltachat-desktop
     tor-browser
     obs-studio
     audacity
     ffmpeg-full
     cifs-utils
     signal-desktop
     gparted
     gimp
     xclip
     libsForQt5.kdenlive
     teamspeak_client
     minecraft
     filezilla
     mpv
  ];
  
  environment.sessionVariables.NIXOS_OZONE_WL = "1";

}
