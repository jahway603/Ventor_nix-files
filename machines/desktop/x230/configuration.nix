{ config, pkgs, ... }:

{

  imports =
  [ 
      ../../../machines/desktop/desktop.nix
  ];
 
  networking.hostName = "x230nix"; # Define your hostname.
#  networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  hardware.bluetooth.enable = true; # enables support for Bluetooth
  hardware.bluetooth.powerOnBoot = true; # powers up the default Bluetooth controller on boot

  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  hardware.opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
  };

}
