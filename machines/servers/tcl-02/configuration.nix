{ config, pkgs, ... }:

{
  imports =
  [ 
      ../../../machines/servers/servers.nix
  ];
 

  # Bootloader weil der Thin Client iwie buggt
  boot.loader.systemd-boot.graceful = true;


  networking.hostName = "tcl-02"; # Define your hostname.

  virtualisation = {
    podman = {
      enable = true;

      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = true;

      # Required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled = true;
    };
  };

  virtualisation.lxd.enable = true;

  services.tailscale.enable = true;
}
