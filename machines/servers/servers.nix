{ config, pkgs, ... }:

{
  imports =
  [ 
      ../../general.nix
  ];

  networking.wireless.enable = false;  # Enables wireless support via wpa_supplicant.

  environment.systemPackages = with pkgs; [
    mailutils
  ];
 
  services.postfix = {
    enable = true;
    relayHost = "smtp-gw.fritz.box";
    config = {
      inet_interfaces = "loopback-only";
    };
  };
 
}
